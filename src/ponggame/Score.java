
package ponggame;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author joaocassamano
 */
public class Score extends Rectangle {
    
    static int WIDTH;
    static int HEIGHT;
    int player1;
    int player2;
    
    Score(int width,int height){
        Score.WIDTH = width;
        Score.HEIGHT = height;
    }
    
    public void draw(Graphics g){
        g.setColor(Color.white);
        g.setFont(new Font("Times",Font.PLAIN,50));
        
        g.drawLine(WIDTH/2, 0, WIDTH/2, HEIGHT);
        
        g.drawString(String.valueOf(player1/10)+String.valueOf(player1%10), WIDTH/2-80, 50);
        g.drawString(String.valueOf(player2/10)+String.valueOf(player2%10), WIDTH/2+20, 50);
    }
    
}
